import { Intermediary } from "../models/models";

export let intermediaries: Intermediary[] = [
  {
    id: 1,
    name: "US Foods",
    createdOn: new Date("05 May 2021 13:25"),
    order: 2,
    intermediaryType: "RANGE",
    pricing: {
      from: 0.08,
      to: 0.1,
      step: 0.02,
    },
  },
  {
    id: 2,
    name: "Whole Foods",
    createdOn: new Date("05 May 2021 13:25"),
    order: 1,
    intermediaryType: "DROPDOWN",
    pricing: [
      {
        option: "Promotion 20%",
        value: 0.2,
      },
      {
        option: "Promotion 50%",
        value: 0.5,
      },
    ],
  },
];

export let data = {
  intermediaries: intermediaries,
};

export function getNewId() {
  let maxId = Math.max.apply(
    Math,
    data.intermediaries.map(function (o) {
      return o.id || 0;
    })
  );
  return (maxId + 1).toString();
}
