import React from 'react';
import { Route, Routes } from "react-router-dom";
import './App.css';
import IntermediariesContainer from './Intermediaries/IntermediariesContainer';
import {data} from "./data/data"
import { NavBar } from "./CommonUI/NavBar";
import { Col, Container, Row } from "react-bootstrap";
import { IntermediaryDetails } from "./Intermediaries/IntermediaryDetails";

export const DataContext = React.createContext({
  data: data,
});

function App() {
  return (
    <div className="App">
      <NavBar />
      <Container className="pt-5">
        <Row>
          <Col>
            <DataContext.Provider value={{ data }}>
              <Routes>
                <Route path="/" element={<IntermediariesContainer />} />
                <Route
                  path="/intermediary/:id"
                  element={<IntermediaryDetails />}
                />
                <Route
                  path="/intermediary/add"
                  element={<IntermediaryDetails />}
                />
                <Route
                  path="/intermediary/:id/edit"
                  element={<IntermediaryDetails />}
                />
              </Routes>
            </DataContext.Provider>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
