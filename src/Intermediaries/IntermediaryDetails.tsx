import React, { useContext, useEffect, useState } from "react";
import { Button, Col, Container, Form, Row } from "react-bootstrap";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { DataContext } from "../App";
import { getNewId } from "../data/data";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Intermediary } from "../models/models";
const defaultIntermiary: Intermediary = { intermediaryType: "RANGE" };

export function IntermediaryDetails() {
  const { id } = useParams();
  const location = useLocation();
  let navigate = useNavigate();

  const dataContext = useContext(DataContext);
  const { data } = dataContext;

  const [item, setItem] = useState<Intermediary>(defaultIntermiary);
  const [isEditMode, setEditMode] = useState<boolean>(
    location.pathname.includes("edit")
  );

  useEffect(() => {
    setEditMode(location.pathname.includes("edit") || !id);
  }, [location, id]);

  useEffect(() => {
    if (id) {
      const currentItem = data.intermediaries.find(
        (i) => i.id?.toString() === id
      );
      currentItem && setItem(currentItem);
    }
  }, [data, id]);

  const updateItem = (name: string, value: any) => {
    setItem({ ...item, [name]: value } as Intermediary);
  };

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    let itemId = id;
    if (!itemId) {
      itemId = getNewId();
      let intermediary = {
        ...item,
        id: parseFloat(itemId),
        createdOn: new Date(),
      };

      data.intermediaries.push(intermediary);
    }

    navigate(`/intermediary/${itemId}`);
    e.preventDefault();
    e.stopPropagation();
  };

  const fromCheck = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    if (target.value === "") return;
    if (item.intermediaryType === "RANGE") {
      if (item.pricing?.to && parseFloat(target.value) >= item.pricing?.to) {
        if (item.pricing?.step) {
          target.value = parseFloat(
            (item.pricing?.to - item.pricing.step).toFixed(6)
          ).toString();
        } else {
          target.value = item.pricing?.to.toString();
        }
      }
      if (item.pricing?.step) {
        let div = (parseFloat(target.value) / item.pricing.step).toString();
        if (
          parseFloat(parseFloat(div).toFixed(6)).toString().indexOf(".") !== -1
        ) {
          target.value = item.pricing?.from.toString();
        }
      }
      if (id && item.pricing?.from) {
        target.value = Math.min(
          parseFloat(target.value),
          item.pricing.from
        ).toString();
      }
    }
  };

  const toCheck = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    if (target.value === "") return;
    if (item.intermediaryType === "RANGE") {
      if (
        item.pricing?.from &&
        parseFloat(target.value) <= item.pricing?.from
      ) {
        if (item.pricing?.step) {
          target.value = parseFloat(
            (item.pricing?.from + item.pricing.step).toFixed(6)
          ).toString();
        } else target.value = item.pricing?.from.toString();
      }
      if (item.pricing?.step) {
        let div = (parseFloat(target.value) / item.pricing.step).toString();
        if (
          parseFloat(parseFloat(div).toFixed(6)).toString().indexOf(".") !== -1
        ) {
          target.value = item.pricing?.to.toString();
        }
      }
    }
  };

  return (
    <Container>
      <Row>
        <Col lg={6}>
          <Form onSubmit={onSubmit} noValidate>
            <Form.Group className="mb-3">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="input"
                name="name"
                maxLength={255}
                placeholder="Enter name"
                defaultValue={item?.name}
                onChange={(e) => updateItem(e.target.name, e.target.value)}
                disabled={!isEditMode}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Order</Form.Label>
              <Form.Control
                type="number"
                name="order"
                min={0}
                placeholder="Order"
                defaultValue={item?.order}
                onKeyPress={(e) =>
                  !/^[0-9]+$/.test(e.key) && e.preventDefault()
                }
                onChange={(e) => updateItem(e.target.name, e.target.value)}
                disabled={!isEditMode}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <Form.Label>Type</Form.Label>
              <Form.Select
                name="intermediaryType"
                value={item?.intermediaryType}
                onChange={(e) => {
                  updateItem(e.target.name, e.target.value);
                }}
                disabled={!isEditMode || id !== undefined}
              >
                <option value="RANGE">Range</option>
                <option value="DROPDOWN">Dropdown</option>
              </Form.Select>
            </Form.Group>

            {item.intermediaryType === "RANGE" && (
              <Row>
                <Form.Group as={Col} md="4" className="mb-3">
                  <Form.Label>From</Form.Label>
                  <Form.Control
                    type="number"
                    name="from"
                    min={0}
                    step={item.pricing?.step}
                    placeholder="From"
                    defaultValue={item?.pricing?.from}
                    onInput={fromCheck}
                    onKeyPress={(e) => {
                      if (
                        e.currentTarget.value !== "" &&
                        !/^\d+([.](\d{1,6})?)?$/.test(e.currentTarget.value)
                      ) {
                        e.preventDefault();
                      }
                    }}
                    onChange={(e) => {
                      updateItem("pricing", {
                        ...item.pricing,
                        from: parseFloat(parseFloat(e.target.value).toFixed(6)),
                      });
                    }}
                    disabled={!isEditMode}
                  />
                </Form.Group>
                <Form.Group as={Col} md="4" className="mb-3">
                  <Form.Label>To</Form.Label>
                  <Form.Control
                    type="number"
                    name="to"
                    min={0}
                    onInput={toCheck}
                    step={item.pricing?.step}
                    placeholder="To"
                    defaultValue={item?.pricing?.to}
                    onKeyPress={(e) => {
                      e.currentTarget.value !== "" &&
                        !/^\d+([.](\d{1,6})?)?$/.test(e.currentTarget.value) &&
                        e.preventDefault();
                    }}
                    onChange={(e) =>
                      updateItem("pricing", {
                        ...item.pricing,
                        to: parseFloat(parseFloat(e.target.value).toFixed(6)),
                      })
                    }
                    disabled={!isEditMode}
                  />
                </Form.Group>
                <Form.Group as={Col} md="4" className="mb-3">
                  <Form.Label>Step</Form.Label>
                  <Form.Control
                    type="number"
                    name="step"
                    min={0}
                    placeholder="Step"
                    defaultValue={item?.pricing?.step}
                    onKeyPress={(e) => {
                      e.currentTarget.value !== "" &&
                        !/^\d+([.](\d{1,6})?)?$/.test(e.currentTarget.value) &&
                        e.preventDefault();
                    }}
                    onChange={(e) =>
                      updateItem("pricing", {
                        ...item.pricing,
                        step: parseFloat(parseFloat(e.target.value).toFixed(6)),
                      })
                    }
                    disabled={!isEditMode}
                  />
                </Form.Group>
              </Row>
            )}

            {item.intermediaryType === "DROPDOWN" && (
              <Col className="mb-2">
                {item.pricing?.map((option, index) => {
                  return (
                    <Row>
                      <Form.Group as={Col} md="4" className="mb-3">
                        {index === 0 && <Form.Label>Option</Form.Label>}
                        <Form.Control
                          type="text"
                          name="option"
                          placeholder="Option"
                          defaultValue={option.option}
                          onChange={(e) => {
                            let newOptions = item.pricing || [];
                            newOptions[index].option = e.target.value;
                            updateItem("pricing", newOptions);
                          }}
                          disabled={!isEditMode}
                        />
                      </Form.Group>
                      <Form.Group as={Col} md="4" className="mb-3">
                        {index === 0 && <Form.Label>Value</Form.Label>}
                        <Form.Control
                          type="number"
                          name="value"
                          placeholder="Value"
                          defaultValue={option.value}
                          onKeyPress={(e) => {
                            e.currentTarget.value !== "" &&
                              !/^\d+([.](\d{1,6})?)?$/.test(
                                e.currentTarget.value
                              ) &&
                              e.preventDefault();
                          }}
                          onChange={(e) => {
                            let newOptions = item.pricing || [];
                            newOptions[index].value = parseFloat(
                              e.target.value
                            );
                            updateItem("pricing", newOptions);
                          }}
                          disabled={!isEditMode}
                        />
                      </Form.Group>
                    </Row>
                  );
                })}

                <button
                  className={`btn btn-primary ${!isEditMode && "disabled"}`}
                  type="button"
                  onClick={() => {
                    let newOptions = item.pricing || [];
                    newOptions.push({ option: "", value: 0 });
                    updateItem("pricing", newOptions);
                  }}
                >
                  <FontAwesomeIcon icon={faPlus as IconProp} />
                </button>
              </Col>
            )}

            {item.createdOn && (
              <Form.Group className="mb-3">
                <label>
                  {/* TODO apply appropriate formatting to date considering timezibe */}
                  <strong>Created on: </strong> {item.createdOn.toString()}
                </label>
              </Form.Group>
            )}

            {isEditMode && (
              <>
                <button className="btn btn-primary me-2" type="submit">
                  Save
                </button>
                <button
                  className="btn btn-secondary"
                  onClick={() => {
                    if (id) navigate(`/intermediary/${id}`);
                    else navigate(`/`);
                  }}
                >
                  Cancel
                </button>
              </>
            )}
            {!isEditMode && (
              <Button
                variant="secondary"
                onClick={() => navigate(location.pathname + "/edit")}
              >
                Edit
              </Button>
            )}
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
