import { IconProp } from "@fortawesome/fontawesome-svg-core";
import {
  faEye,
  faPenToSquare,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useContext } from "react";
import { Button, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import { DataContext } from "../App";
import { Intermediary } from "../models/models";

function IntermediariesContainer() {
  const dataContext = useContext(DataContext);
  const { data } = dataContext;

  return (
    <>
      <Link to="intermediary/add">
        <Button variant="success" className="mb-2">
          Add new
        </Button>
      </Link>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Created at</th>
            <th>Name</th>
            <th>Order</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {data.intermediaries
            .sort((a: Intermediary, b: Intermediary) => {
              if (a.order && b.order && a.order > b.order) {
                return 1;
              } else return -1;
            })
            .map((item) => {
              return (
                <tr>
                  {/* TODO apply appropriate formatting to date considering timezibe */}
                  <td>{item.createdOn?.toString()}</td>
                  <td>{item.name}</td>
                  <td>{item.order}</td>
                  <td>
                    <div className="d-flex justify-content-md-around">
                      <Link to={`/intermediary/${item.id}`}>
                        <FontAwesomeIcon icon={faEye as IconProp} />
                      </Link>
                      <Link to={`/intermediary/${item.id}/edit`}>
                        <FontAwesomeIcon icon={faPenToSquare as IconProp} />
                      </Link>
                      <Link to="" onClick={() => alert("Delete")}>
                        <FontAwesomeIcon icon={faTrash as IconProp} />
                      </Link>
                    </div>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </Table>
    </>
  );
}

export default IntermediariesContainer;
