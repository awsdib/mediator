export type IntermediaryCommon = {
  id?: number;
  name?: string;
  createdOn?: Date;
  order?: number;
};

export type IntermediaryRange = IntermediaryCommon & {
  intermediaryType: "RANGE";
  pricing?: RangePricing;
};

export type IntermediaryDropdown = IntermediaryCommon & {
  intermediaryType: "DROPDOWN";
  pricing?: DropdownPricing[];
};

export type Intermediary = IntermediaryRange | IntermediaryDropdown;

export type RangePricing = {
  from: number;
  to: number;
  step: number;
};

export type DropdownPricing = {
  option: string;
  value: number;
};
